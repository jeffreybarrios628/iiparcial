﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial
{
    class ProductosVend
    {

        string cod;
        string nombre_producto;
        string descripcion;
        double cantidad;
        double precio;
        double total;

        public ProductosVend(string cod, string nombre_producto, string descripcion, double cantidad, double precio, double total)
        {
            this.cod = cod;
            this.nombre_producto = nombre_producto;
            this.descripcion = descripcion;
            this.cantidad = cantidad;
            this.precio = precio;
            this.total = total;
        }

        public string Cod
        {
            get
            {
                return cod;
            }

            set
            {
                cod = value;
            }
        }

        public string Nombre_producto
        {
            get
            {
                return nombre_producto;
            }

            set
            {
                nombre_producto = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public double Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }
    }
}
