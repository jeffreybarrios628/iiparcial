﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class ProductosVendidos : Form
    {
        public ProductosVendidos()
        {
            InitializeComponent();
        }
        private DataSet dsSistema;
        FrmFactura f = new FrmFactura();
        public DataSet DsSistema
        {
            get
            {
                return dsSistema;
            }

            set
            {
                dsSistema = value;
            }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)f.cmbProductos.SelectedItem).Row;
                DataRow drProductoFactura = DsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                DsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "ERROR, producto ya agregado, verifique por favor!",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
